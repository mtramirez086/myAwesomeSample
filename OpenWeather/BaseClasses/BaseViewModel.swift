//
//  BaseViewModel.swift
//  OpenWeather
//
//  Created by Macky Ramirez on 3/6/20.
//  Copyright © 2020 Macky Ramirez. All rights reserved.
//
import Foundation
import NVActivityIndicatorView
protocol ViewModelDelegate: class {
    func didRequestSuccess()
    func didRequestFailed(error: Error)
}
extension ViewModelDelegate {
    func didRequestSuccess() { }
    func didRequestFailed(error: Error) { }
}
class BaseViewModel: NSObject {
    weak var delegate: ViewModelDelegate?
    let activityData = ActivityData()
    init(_ delegate: ViewModelDelegate? = nil) {
        super.init()
        self.delegate = delegate
    }
}
extension BaseViewModel: NVActivityIndicatorViewable {
    func startAnimating() {
        NVActivityIndicatorView.DEFAULT_TYPE = .ballScaleRipple
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityData)
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Loading please wait...")
    }
    func stopAnimating() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
}
