//
//  WeatherViewModel.swift
//  OpenWeather
//
//  Created by Macky Ramirez on 3/1/20.
//  Copyright © 2020 Macky Ramirez. All rights reserved.
//
import Foundation
import Moya
class WeatherViewModel: BaseViewModel, WeatherProtocol {
    fileprivate var tableView: UITableView!
    // MARK: NETWORK CALL
    let provider = MoyaProvider<WeatherService>()
    func getWeatherThreeHrForecast(city:String,
                                   success successCallback: @escaping (_ responseData: WeatherResponseModel) -> Void,
                                   failure failureCallback: @escaping (_ moyaerror: MoyaError) -> Void) {
        print("\(Constant.Token)")
        provider.request(.threeHrForecast(city: city,
                                   apiKey: Constant.Token)) { (result) in
                                    switch result {
                                    case let .success(response):
                                        let data = response.data
                                        let statusCode = response.statusCode
                                        print("\(statusCode)")
                                    do {
                                        let weather = try WeatherResponseModel.init(data: data)
                                        successCallback(weather)
                                    } catch {
                                        failureCallback(error as! MoyaError)
                                    }
                                    case let .failure(error):
                                        print("\(error)")
                                        failureCallback(error)
                                    }
        }
    }
    func serviceRequest(_ query: String) {
        self.startAnimating()
        getWeatherThreeHrForecast(city: query, success: { (response) in
            self.weatherlist.removeAll()
            self.loadData(weatherData: response)
            self.delegate?.didRequestSuccess()
            self.stopAnimating()
            self.getThreeHrInBackground(query)
        }, failure: { (error) in
            print("\(error)")
            self.delegate?.didRequestSuccess()
            self.stopAnimating()
        })
    }
    // MARK: PROPERTIES
    var weatherlist = [WeatherList]()
    var weatherDetails = [WeatherDetail]()
    var weatherCityDetails: WeatherCity?
    var weatherWindDetails: WeatherWind?
    var weatherCloudDetails: WeatherCloud?
    var weatherMainDetails: WeatherMain?
    // MARK: MAPPING OF RESPONSE DATA
    func loadData(weatherData: WeatherResponseModel) {
        // MARK: CITY
        weatherCityDetails = weatherData.city.map { WeatherCity.init(cityId: $0.id,
                                                                     cityName: $0.name,
                                                                     weatherCountry: $0.country) }
        weatherData.list?.forEach {
            // MARK: WEATHER MAIN
            $0.main.map { (mainDetails) in
                weatherMainDetails = WeatherMain.init(weatherTemp: mainDetails.temp,
                                                      weatherPressure: mainDetails.pressure,
                                                      weatherHumid: mainDetails.humidity)
            }
            // MARK: WEATHER DETAILS
            $0.weather?.forEach { (info) in
                weatherDetails.append( WeatherDetail.init(weatherId: info.id,
                                                          weatherMain: info.main.map { $0.rawValue },
                                                          weatherDesc: info.weatherDescription,
                                                          weatherIcon: info.icon) )
            }
            // MARK: WIND
            $0.wind.map { (windDetail) in
                weatherWindDetails = WeatherWind.init(windSpeed: windDetail.speed,
                                                      windDegree: windDetail.deg)
            }
            // MARK: CLOUD
            $0.clouds.map { (cloudDetail) in
                weatherCloudDetails = WeatherCloud.init(cloudPercentage: cloudDetail.all)
            }
            // MARK: LIST
            weatherlist.append( WeatherList.init(weatherDate: $0.dtTxt,
                                                 weatherMain: weatherMainDetails,
                                                 details: weatherDetails,
                                                 windDetails: weatherWindDetails,
                                                 weatherCloud: weatherCloudDetails,
                                                 city: weatherCityDetails) )
        }
    }
    //
    // MARK: TO DO
    // MARK: DATE COMPARE
    // MARK: CORE DATA STORAGE
    //
    // MARK: Timer for Network Polling
    // OneHr 3600.0
    // ThreeHr 10800.0
    var repeatTime = RepeatingTimer(timeInterval: 3600.0)
    func getThreeHrInBackground(_ query: String) {
        repeatTime.eventHandler = {
            self.getWeatherThreeHrForecast(city: query, success: { (response) in
                self.loadData(weatherData: response)
                self.delegate?.didRequestSuccess()
            }, failure: { (error) in
                print("\(error)")
                self.repeatTime.suspend()
                self.delegate?.didRequestSuccess()
            })
        }
        repeatTime.resume()
    }
    // MARK: TABLEVIEWCELL
//    func cellForRow(cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastTableViewCell",
//                                                 for: indexPath) as? ForecastTableViewCell
//        cell?.setDteFrom(getweatherDate(indexPath.row))
//        cell?.setImageFrom(getweatherIcon(indexPath.row))
//        return cell ?? UITableViewCell()
//    }
}
extension Array {
    func unique<T:Hashable>(map: ((Element) -> (T))) -> [Element] {
        var set = Set<T>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(map(value)) {
                set.insert(map(value))
                arrayOrdered.append(value)
            }
        }

        return arrayOrdered
    }
}
