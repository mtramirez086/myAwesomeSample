//
//  ViewController.swift
//  OpenWeather
//
//  Created by Macky Ramirez on 3/1/20.
//  Copyright © 2020 Macky Ramirez. All rights reserved.
//
import UIKit
class ViewController: UIViewController, ViewModelDelegate {
    let viewModel =  WeatherViewModel()
    @IBOutlet weak var searchBar: UISearchBar! {
           didSet {
               searchBar.delegate = self
           }
       }
    @IBOutlet weak var tableview: UITableView! {
        didSet {
            tableview.delegate = self
            tableview.dataSource = self
            let bundle = Bundle(for: type(of: self))
            let nib = UINib(nibName: "ForecastTableViewCell", bundle: bundle)
            tableview.register(nib, forCellReuseIdentifier: "ForecastTableViewCell")
            tableview.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
            tableview.rowHeight = UITableView.automaticDimension
            tableview.estimatedRowHeight = 100
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        viewModel.delegate = self
//        viewModel.serviceRequest()
    }
    func didRequestSuccess() {
        DispatchQueue.main.async {
            self.tableview.reloadData()
        }
    }
    func didRequestFailed(error: Error) {
        print(error)
        DispatchQueue.main.async {
            self.tableview.reloadData()
        }
    }
}
extension ViewController: UISearchBarDelegate {
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.viewModel.serviceRequest(searchBar.text ?? "")
        searchBar.resignFirstResponder()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.viewModel.serviceRequest(searchBar.text ?? "")
        searchBar.resignFirstResponder()
    }
}
extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfWeatherListInSection(section)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        return self.viewModel.cellForRow(cellForRowAt: indexPath)
        // MARK: SWITCH THIS TO VIEWMODEL
        let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastTableViewCell",
                                                 for: indexPath) as? ForecastTableViewCell
        cell?.setDteFrom(self.viewModel.getweatherDate(indexPath.row))
        cell?.setImageFrom(self.viewModel.getweatherIcon(indexPath.row))
        cell?.setDesc(self.viewModel.getweatherDesc(indexPath.row))
        cell?.setClouds(self.viewModel.getCloudPercentage(indexPath.row))
        cell?.setMaxTemp(self.viewModel.getTemp(indexPath.row))
        cell?.setWindSpeed(self.viewModel.getwindSpeed(indexPath.row))
        cell?.setPressure(self.viewModel.getPressure(indexPath.row))
        return cell ?? UITableViewCell()
    }
}
