//
//  WeatherProtocol.swift
//  OpenWeather
//
//  Created by Macky Ramirez on 3/5/20.
//  Copyright © 2020 Macky Ramirez. All rights reserved.
//
import Foundation
protocol WeatherProtocol {
    var weatherlist: [WeatherList] {get set}
    var weatherCityDetails: WeatherCity? {get set}
    func numberOfWeatherListInSection(_ section: Int) -> Int
    func getweatherDate(_ index: Int) -> String
    func getweatherCityName() -> String
    func getweatherCountry() -> String
    func getweatherMain(_ index: Int) -> String
    func getweatherDesc(_ index: Int) -> String
    func getweatherIcon(_ index: Int) -> String
    func getwindSpeed(_ index: Int) -> String
    func getwindDeg(_ index: Int) -> String
    func getCloudPercentage(_ index:Int) -> String
    func getTemp(_ index: Int) -> String
    func getPressure(_ index: Int) -> String
    func getHumid(_ index: Int) -> String
}
extension WeatherProtocol {
    func numberOfWeatherListInSection(_ section: Int) -> Int {
        guard section < self.weatherlist.count else { return 0 }
        return self.weatherlist.count
    }
    func getweatherDate(_ index: Int) -> String {
        guard let dteString = weatherlist[index].weatherDate else { return "" }
//        let dateFormatter = DateFormatter()
//        dateFormatter.locale = .current
//        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
//        dateFormatter.dateFormat = "EEE, dd-MM-yyyy"
//        let dteConverted = dateFormatter.date(from: dteString) ?? Date()
//        var time: TimeInterval = dteConverted.timeIntervalSinceReferenceDate/1000
//        var finalDate = Date(timeIntervalSinceNow: time)
//        let now = dateFormatter.string(from: dteConverted)
        return String("\(dteString)")
    }
    func getweatherCityName() -> String {
        guard let name = weatherCityDetails?.cityName else { return "" }
        return String("\(name)")
    }
    func getweatherCountry() -> String {
        guard let country = weatherCityDetails?.weatherCountry else { return "" }
        return String("\(country)")
    }
    func getweatherMain(_ index: Int) -> String {
        guard let weatherMain = weatherlist[index].details?[index].weatherMain else { return "" }
        return weatherMain
    }
    func getweatherDesc(_ index: Int) -> String {
        guard let desc = weatherlist[index].details?[index].weatherDesc else { return "" }
        return desc
    }
    func getweatherIcon(_ index: Int) -> String {
        guard let weatherIcon = weatherlist[index].details?[index].weatherIcon else { return "" }
        return weatherIcon
    }
    func getwindSpeed(_ index: Int) -> String {
        guard let speed = weatherlist[index].windDetails?.windSpeed else { return "" }
        return String("\(speed)")
    }
    func getwindDeg(_ index: Int) -> String {
        guard let deg = weatherlist[index].windDetails?.windDegree else { return "" }
        return String("\(deg)")
    }
    func getCloudPercentage(_ index:Int) -> String {
        guard let all = weatherlist[index].weatherCloud?.cloudPercentage else { return "" }
        return String("\(all)")
    }
    func getTemp(_ index: Int) -> String {
        guard let temp = weatherlist[index].weatherMain?.weatherTemp else { return "" }
        return String("\(temp)")
    }
    func getPressure(_ index: Int) -> String {
        guard let pressure = weatherlist[index].weatherMain?.weatherPressure else { return "" }
        return String("\(pressure)")
    }
    func getHumid(_ index: Int) -> String {
        guard let humid = weatherlist[index].weatherMain?.weatherHumid else { return "" }
        return String("\(humid)")
    }
}
