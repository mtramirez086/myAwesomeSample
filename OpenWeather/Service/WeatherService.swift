//
//  WeatherService.swift
//  OpenWeather
//
//  Created by Macky Ramirez on 3/1/20.
//  Copyright © 2020 Macky Ramirez. All rights reserved.
//
import Foundation
import Moya
enum WeatherService {
    case threeHrForecast(city: String, apiKey: String)
}
extension WeatherService: TargetType {
    var baseURL: URL { return URL(string: Keys.BaseUrl)! }
    var path: String {
        switch self {
        case .threeHrForecast:
            return Paths.Forcast
        }
    }
    var method: Moya.Method {
        switch self {
        case .threeHrForecast:
            return .get
        }
    }
    var task: Task {
        switch self {
        case let .threeHrForecast(city, apiKey):
            return .requestParameters(parameters: ["q":city, "appid":apiKey],
                                      encoding: URLEncoding.queryString)
        }
    }
    var headers: [String : String]? {
        ["Content-type": "application/json"]
    }
    var sampleData: Data {
        switch self {
        case .threeHrForecast:
            guard let url = Bundle.main.url(forResource: "sample", withExtension: "json"),
                let data = try? Data(contentsOf: url) else { return Data() }
            return data
        }
    }
}
// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }

    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
