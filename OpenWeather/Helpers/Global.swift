//
//  Contants.swift
//  OpenWeather
//
//  Created by Macky Ramirez on 3/1/20.
//  Copyright © 2020 Macky Ramirez. All rights reserved.
//

import Foundation
struct Constant {
    static let Token   = "229693af8af6c33ccf53ccd1f9c4c6d9"
}
struct Keys {
    static let BaseUrl = "https://api.openweathermap.org/data/2.5/"
    static let IconUrl = "https://openweathermap.org/img/wn/"
}
struct Paths {
    static let Forcast = "forecast"
    static let imgEnd  = "@2x.png"
}
