//
//  RepeatingTimer.swift
//  OpenWeather
//
//  Created by Macky Ramirez on 3/5/20.
//  Copyright © 2020 Macky Ramirez. All rights reserved.
//
import Foundation
class RepeatingTimer {
    let timeInterval: TimeInterval
    init(timeInterval: TimeInterval) { self.timeInterval = timeInterval }
    private lazy var timer: DispatchSourceTimer = {
        let tme = DispatchSource.makeTimerSource()
        tme.schedule(deadline: .now() + self.timeInterval, repeating: self.timeInterval)
        tme.setEventHandler(handler: { [weak self] in
            self?.eventHandler?()
        })
        return tme
    }()
    var eventHandler: (() -> Void)?
    private enum State {
        case suspended
        case resumed
    }
    private var state: State = .suspended
    deinit {
        timer.setEventHandler { }
        timer.cancel()
        /*
         If the timer is suspended, calling cancel without resuming
         triggers a crash. This is documented here https://forums.developer.apple.com/thread/15902
         */
        resume()
        eventHandler = nil
    }
    func resume() {
        if state == .resumed { return }
        state = .resumed
        timer.resume()
    }
    func suspend() {
        if state == .suspended { return }
        state = .suspended
        timer.suspend()
    }
}
