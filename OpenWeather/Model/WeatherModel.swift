//
//  WeatherModel.swift
//  OpenWeather
//
//  Created by Macky Ramirez on 3/1/20.
//  Copyright © 2020 Macky Ramirez. All rights reserved.
//
import Foundation
struct WeatherList {
    var weatherDate: String?
    var weatherMain: WeatherMain?
    var details: [WeatherDetail]?
    var windDetails: WeatherWind?
    var weatherCloud: WeatherCloud?
    var city: WeatherCity?
}
struct WeatherMain {
    var weatherTemp: Double?
    var weatherPressure: Double?
    var weatherHumid: Double?
}
struct WeatherWind {
    var windSpeed: Double?
    var windDegree: Int?
}
struct WeatherCloud {
    var cloudPercentage: Int?
}
struct WeatherDetail {
    var weatherId: Int?
    var weatherMain: String?
    var weatherDesc: String?
    var weatherIcon: String?
}
struct WeatherCity {
    var cityId: Int?
    var cityName: String?
    var weatherCountry: String?
}
