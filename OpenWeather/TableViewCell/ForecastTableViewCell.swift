//
//  ForecastTableViewCell.swift
//  OpenWeather
//
//  Created by Macky Ramirez on 3/6/20.
//  Copyright © 2020 Macky Ramirez. All rights reserved.
//
import UIKit
import Foundation
import Kingfisher
class ForecastTableViewCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var dteLabel: UILabel!
    @IBOutlet weak var tempMax: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var windSpeed: UILabel!
    @IBOutlet weak var clouds: UILabel!
    @IBOutlet weak var pressure: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgView.backgroundColor = .lightGray
    }
    internal func setDteFrom(_ dte: String) {
        self.dteLabel.text = String("\(dte)")
    }
    internal func setImageFrom(_ urlString: String) {
        if urlString.isEmpty {
            imgView.image = UIImage(named: "")
        } else {
            let str = Keys.IconUrl + urlString + Paths.imgEnd
            print("\(str)")
            let url = URL(string: str)
            imgView.kf.setImage(with: url)
        }
    }
    internal func setMaxTemp(_ maxTemp: String) {
        self.tempMax.text = String("\(maxTemp) C")
    }
    internal func setDesc(_ desc: String) {
        self.desc.text = desc
    }
    internal func setWindSpeed(_ speed: String) {
        self.windSpeed.text = String("Wind: \(speed) m/s")
    }
    internal func setClouds(_ clouds: String) {
        self.clouds.text = String("Clouds: \(clouds) %")
    }
    internal func setPressure(_ pressure: String) {
        self.pressure.text = String("\(pressure) hpa")
    }
}
